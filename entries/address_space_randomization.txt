address space randomization

The practice or randomly changing the layout of memory so as to avoid exploits
which simply poke some value to a fixed memory offset. Used by the Linux kernel
for hardening security against exploitation.