balaclava man

Fictional character. The most legendary hacker. Often depicted in
mainstream news stories about hackers, he is hunched over a laptop
wearing a black balaclava or hoodie. Occasionally balaclava man wears
biker gloves and sometimes he is also a woman but mostly is depicted
as being of masculine gender. Sometimes he is doing something strange
to the laptop keyboard, such as trying to hammer it or bash it with
a crowbar. This is what the mainstream media thinks hackers look like.