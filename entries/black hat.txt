black hat

1. n. A criminal, malicious or offensive security oriented hacker; a cracker.
Contrast with white hat. 2. Name of a conference about offensive hacking.
Also known as red teaming.
