eye candy

/i: kand`ee/ , n. [from mainstream slang ear candy ] A display of some sort.
In mainstream usage among players of graphics-heavy computer games. Can also
be used in the context of desktop operating system appearance, as in:
"KDE Plasma has a lot of eye candy". Also see bling.
