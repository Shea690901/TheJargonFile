newbie

/n[y]oobee/ , n. A new user of some system. This term surfaced in the
Usenet newsgroup talk.bizarre but is now in wide use (the combination clueless
newbie is especially common). Criteria for being considered a newbie vary
wildly; a person can be called a newbie in one newsgroup while remaining a
respected regular in another. The label newbie is sometimes applied as a
serious insult to a person who has been around Usenet for a long time but
who carefully hides all evidence of having a clue. See B1FF; see also
gnubie. Compare chainik.
