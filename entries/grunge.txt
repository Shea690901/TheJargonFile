grunge

/gruhnj/ , n. 1. That which accumulates under the keys of a keyboard and
requires periodic cleaning. 2. [Cambridge] Code which is inaccessible
due to changes in other parts of the program. The preferred term in
North America is dead code.
