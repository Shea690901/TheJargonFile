google juice

n. A hypothetical substance which attracts the index bots of Google. In
common usage, a web page or web site with high placement in the results of a
particular search on Google or frequent placement in the results of a
various searches is said to have a lot of google juice.
See also juice, google.
