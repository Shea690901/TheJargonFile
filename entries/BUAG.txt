BUAG

// , n. [abbreviation, from alt.fan.warlord] Big Ugly ASCII Graphic.
Pejorative term for ugly ASCII art, especially as found in sig blocks. For
some reason, mutations of the head of Bart Simpson are particularly common
in the least imaginative sig blocks. See warlording.
