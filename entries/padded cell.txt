padded cell

Also known as jailware. Proprietary software is often a padded cell.
A padded cell may be nice in appearance. It may have attractive features
and some convenience factor, but it still robs the user of essential
freedoms. Much Apple software is like this. High on bling, low on freedom.