GIGO

/gi:goh/ Garbage In, Garbage Out usually said in response to users who
complain that a program didn't do the right thing when given imperfect input
or otherwise mistreated in some way. Also commonly used to describe failures
in human decision making due to faulty, incomplete, or imprecise data.